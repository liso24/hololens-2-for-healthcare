# HoloLens 2 for healthcare
Progetto Unity con componenti MRTK utilizzato per:

* Visualizzazione file DICOM
* Visualizzazione e manipolazione modelli 3D
* Confronto tra DICOM file e modelli 3D

## Versioni utilizzate
* Unity 2020.3.4f1
* Plugin WIndows XR 4.4.2
* Visual Studio 2019

## Descrizione
All'interno della scena di Unity e del progetto sono presenti:

* Un modello 3D di Reni.
* Un DICOM visualizzato a sprite, non ancora allineato e messo a confronto con il modello 3D.
* La libreria a momento utilizzata fo-dicom.
* La libreria precedente a quella attualmente in utilizzo evilDICOM.

## Funzionamento e problematiche
Lanciando l'applicazione attraverso la preview di Unity, si potrà notare come è presente un file DICOM visualizzato come sprite all'interno dell'ambiente di lavoro.
Tutto i componenti del progetto e gli script si trovano all'interno della cartella "Resources".
Nonostante il corretto funzionamento in preview e la possibilità di manipolare l'oggetto attraverso il toolkit messo a disposizione da Microsoft,
quando viene effettuato il deploy su Hololens la libreria sembra non funionare (nessuna delle 2).
A seguito di tentativi si è giunti alla conclusione che i possibili problemi del malfunzionamento siano:

* Hololens ha problemi con il deploy delle dll, di entrambe librerie in quanto entrambe non funzionanti.
* Lo script che si occupa di prendere il file DICOM e manipolarlo, non ritrova correttamente quest'ultimo all'interno delle directory a seguito del deploy
