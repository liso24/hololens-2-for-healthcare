﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for extracting a Unity texture from a given dataset. 
/// It contains the decoding pixel data logic.
/// </summary>
public class PixelProcessor
{
    public DICOMDataset DicomDataset { get; set; }

    public PixelProcessor(DICOMDataset dd)
    {
        this.DicomDataset = dd;
    }

    //to add in the future:
    //- checking for compressed pixels (by analyzing TransferSyntax)
    //- checking for multiframe image (NumberOfFrames (0028, 0008))
    public Texture2D GetFrameAsTexture()
    {
        Texture2D retText;
        string photo = (string)DicomDataset.FindFirst(ImageProcessingTagHelper.Photometric​Interpretation()).GetData();

        if (photo.Contains("MONOCHROME")){
            AImage<byte> res = new DICOMGreyscalePipeline(new GreyscaleParameters(DicomDataset), ExtractBytePixelData()).Apply();
            retText = new DICOMGreyscaleImage(res, res.Width, res.Height).ConvertToTexture();
        }
        else //RGB or color palette encoding
        {
            throw new NotImplementedException("RGB and palette photometric interpretations not supported yet.");
        }

        return retText;
    }

    private AImage<ushort> ExtractBytePixelData()
    {
        //here goes the extraction of pixels without elaboration
        ushort bitsAllocated = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.BitsAllocated()).GetData();
        ushort highBit = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.HighBit()).GetData();
        ushort bitsStored = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.BitsStored()).GetData();
        ushort rows = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.Rows()).GetData();
        ushort columns = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.Columns()).GetData();
        ushort pixelRepresentation = (ushort)DicomDataset.FindFirst(ImageProcessingTagHelper.PixelRepresentation()).GetData();
        List<byte> pixelData = (List<byte>)DicomDataset.FindFirst(ImageProcessingTagHelper.PixelData()).GetMultipleData();

        ushort[] outPixelData = new ushort[rows * columns];//rgba
        ushort mask = (ushort)(ushort.MaxValue >> (bitsAllocated - bitsStored));
        double maxval = Math.Pow(2, bitsStored);



        for (int i = 0, j=0 ; i < pixelData.Count; i += 2, j ++)
        {
            ushort gray = (ushort)((ushort)(pixelData[i]) + (ushort)(pixelData[i + 1] << 8));
            double valgray = gray & mask;//remove not used bits

            if (pixelRepresentation == 1)// the last bit is the sign, apply a2 complement
            {
                if (valgray > (maxval / 2))
                {
                    valgray = (valgray - maxval);
                }
            }
            outPixelData[j] = (ushort)valgray;
        }

        return new AImage<ushort>(outPixelData, rows, columns);
    }
}
