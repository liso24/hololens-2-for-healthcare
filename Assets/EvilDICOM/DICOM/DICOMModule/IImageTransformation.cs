﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IImageTransformation<T>
{
    AImage<T> Apply();
}
