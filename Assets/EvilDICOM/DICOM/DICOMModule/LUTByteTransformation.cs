﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LUTByteTransformation : IImageTransformation<byte>
{
    private AImage<ushort> input;
    private AImage<byte> lut;
    private AImage<byte> output;

    //devo implementare anche il costruttore che prende la lut già pronta, estratta dal DICOM

    public LUTByteTransformation(AImage<ushort> input, PixelFunction<ushort, byte> pf)
    {
        this.input = input;//deep copy della input? no, tanto non la modifico
        lut = new AImage<byte>(32768, 2);
        output = new AImage<byte>(input.Width, input.Height);
        for (ushort i = 0; i < 65535; i++)
        {
            byte ris = pf(i);
            lut[i] = ris > 255 ? (byte)255 : ris;
        }
        
    }

    public AImage<byte> Apply()
    {
        for (int i = 0; i < output.Width * output.Height; i++)
        {
            output[i] = lut[input[i]];
        }
        return output;
    }
}
