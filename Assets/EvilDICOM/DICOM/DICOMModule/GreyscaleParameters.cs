﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GreyscaleParameters
{

    public double Intercept { get;  set;}  //default = 0
    public double Slope { get; set; }    //default = 1.0
    public double Window { get; set; }
    public double Level { get; set; }
    //should include other DICOM parameters

    public GreyscaleParameters(DICOMDataset dds)
    {
        Intercept = (double)(dds.FindFirst(ImageProcessingTagHelper.RescaleIntercept()).isNull() ? 0.0 : dds.FindFirst(ImageProcessingTagHelper.RescaleIntercept()).GetData());
        Slope = (double)(dds.FindFirst(ImageProcessingTagHelper.RescaleSlope()).isNull() ? 1.0 : dds.FindFirst(ImageProcessingTagHelper.RescaleSlope()).GetData());
        Window = (double)(dds.FindFirst(ImageProcessingTagHelper.WindowWidth()).GetData());
        Level = (double)(dds.FindFirst(ImageProcessingTagHelper.WindowCenter()).GetData());
    }

    public string DebugString()
    {
        return "Intercept:" + Intercept + "Slope: " + Slope + " Window: " + Window + ", Level: " + Level;
    }
}
