﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class UnityImage<T> : AImage<T>
{
    public UnityImage(int width, int height) : base(width, height)
    {
    }

    public UnityImage(T[] pixelArray, int width, int height) : base(pixelArray, width, height)
    {
    }

    public abstract Texture2D ConvertToTexture();
}
