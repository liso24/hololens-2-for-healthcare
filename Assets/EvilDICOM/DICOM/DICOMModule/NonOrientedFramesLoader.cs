﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is a loader that, given the DICOM directory path containing the DICOM datasets, outputs
/// the acquisition slices, without the spatial relations nedded to position them in the "patient coordinates
/// system".
/// </summary>
public class NonOrientedFramesLoader : AcquisitionFramesLoader
{
    private Vector3 position;
    public NonOrientedFramesLoader(string directoryName, Vector3 position) : base(directoryName)
    {
        this.position = position;
    }

    protected override DICOMFrame LoadFrame(string filename)
    {
        Debug.Log("dentro a LoadFrames");
        float fromMetersToMillimeters = 1 / 1000.0f;
        var a = new DICOMFrame(pp.GetFrameAsTexture(), new Transformation3D(position, new Vector3(0, 0, 0), new Vector3(fromMetersToMillimeters * 10, fromMetersToMillimeters * 10, 1), new Vector2(256.0f, 256.0f)));
        
        return new DICOMFrame(pp.GetFrameAsTexture(), new Transformation3D(position, new Vector3(0, 0, 0), new Vector3(fromMetersToMillimeters, fromMetersToMillimeters, 1), new Vector2(256.0f, 256.0f)));
    }
}
