﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFrameLoader
{
    void LoadFrames();  //no need modifiers as they all public by def in interface

    IList<DICOMFrame> GetFrames();
}
