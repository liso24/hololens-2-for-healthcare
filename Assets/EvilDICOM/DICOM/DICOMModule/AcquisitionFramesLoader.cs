﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

/// <summary>
/// This class models a loader that, given the DICOM directory path containing the DICOM datasets, outputs
/// the acquisition slices, that are the DICOM images directly contained in the files. The responsibility for
/// embedding them with spatial relations extracted from DICOM or not is in charge of subclasses.
/// </summary>
public abstract class AcquisitionFramesLoader : IFrameLoader
{ 
    private string directoryPath;
    private IList<DICOMFrame> frames;
    protected PixelProcessor pp;        //should have a protected method and a private field instead

    public AcquisitionFramesLoader(string directoryPath)    //could also be singleton
    {
        this.directoryPath = directoryPath;
        frames = new List<DICOMFrame>();
    }

    public void LoadFrames()
    {
        //to add in the future:
        //- checking for DICOM files with extension (.dcm, .dc3, .dic) - now looks only for files without extension
        //- sorting files by dicom slices order

        var filesWithoutExtension = System.IO.Directory.GetFiles(directoryPath).Where(filPath => System.String.IsNullOrEmpty(System.IO.Path.GetExtension(filPath)));
        foreach (string path in filesWithoutExtension)
        {
            pp = new PixelProcessor(new DICOMDataset(path));
            frames.Add(LoadFrame(path));
        }
    }

    public IList<DICOMFrame> GetFrames()
    {
        return frames;
    }

    protected abstract DICOMFrame LoadFrame(string filename);
}
