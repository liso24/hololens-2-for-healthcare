﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class models the DICOM Greyscale Pipeline, which is made by the elaboration steps that the pixels
/// pf greyscale images are subject to, according to the Softcopy Presentation State Storage SOP Class, defined
/// by the DICOM standard.
/// </summary>
public class DICOMGreyscalePipeline
{
    private ModalityTransformation mt;
    private VOITransformation vt;
    AImage<ushort> input;

    public DICOMGreyscalePipeline(GreyscaleParameters gp, AImage<ushort> input)
    {
        mt = new ModalityTransformation(gp, input);
        vt = new VOITransformation(gp, mt.Apply());
    }

    public AImage<byte> Apply()
    {
        return vt.Apply();
    }
}
