﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

/// <summary>
/// This script manages the intersection of DICOM slices extracted from a DICOM folder with the 3D
/// model got from the same files. It handles the logic of:
/// - displaying the DICOM frame chosen by the user by the means of a slider.
/// - orienting the frame chosen according to patient coordinates system extracted from DICOM
/// - cutting the upper part of the mesh of the model resulting from the intersection with the chosen slice.
/// - dinamically adjust these coordinates according to the position of the model.
/// </summary>
public class ClippingModelFramesController : MonoBehaviour
{
    public Slider indexSlider;  //this is the gameobject, not the script
    public GameObject segmentedModel;

    private IList<DICOMFrame> frames;
    private DICOMFrame df;
    private Vector3 startingPos, startingRot;

    void Start()
    {
        //var DICOMDIRPath = Path.Combine(Application.persistentDataPath, "DICOM");   //this is the directory wheredicom files are stored
        var DICOMDIRPath = Application.dataPath + "\\MyAssets\\DICOM";

        var myFl = new OrientedFramesLoader(DICOMDIRPath);
        myFl.LoadFrames();
        frames = myFl.GetFrames();
       
        DisplayFrame(frames[0]);

        indexSlider.GetComponent<Slider>().maxValue = 0;    //setto il min e max del slider
        indexSlider.GetComponent<Slider>().maxValue = frames.Count - 1;
        indexSlider.onValueChanged.AddListener(
             delegate { ValueUpdate(); }
        );

        segmentedModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Custom/StandardClippable"))
        {
            hideFlags = HideFlags.HideAndDontSave
        };
        
    }

    private void ValueUpdate()
    {
        DisplayFrame(frames[(int)indexSlider.value]);
    }

    private void DisplayFrame(DICOMFrame df)
    {
        this.df = df;
        GetComponent<SpriteRenderer>().sprite = df.Sprite;
        transform.position = df.Transformation.Position;
        transform.rotation = Quaternion.Euler(df.Transformation.Rotation);
        transform.localScale = new Vector3(df.Transformation.Scale.x * df.Sprite.pixelsPerUnit, df.Transformation.Scale.y * df.Sprite.pixelsPerUnit, 1);//local scale has additional parameters (rows and columns) for bypassing Unity non - supporting for NPOT at run-time (not required if using quads + textures for

    }

    public void Update()
    {
        Debug.Log("lo slider ha valore: " + (int)indexSlider.value);
        Debug.Log("il frame e': " + this.df);
        Debug.Log("df.Transformation" + this.df.Transformation);

        transform.position = new Vector3(df.Transformation.Position.x + segmentedModel.transform.position.x, df.Transformation.Position.y + segmentedModel.transform.position.y, df.Transformation.Position.z + segmentedModel.transform.position.z);
        transform.rotation = Quaternion.Euler(new Vector3(df.Transformation.Rotation.x + segmentedModel.transform.rotation.x, df.Transformation.Rotation.y + segmentedModel.transform.rotation.y, df.Transformation.Rotation.z + segmentedModel.transform.rotation.z));
        // transform.position = segmentedModel.transform.position;
        // transform.rotation = segmentedModel.transform.rotation;
        var sharedMaterial = segmentedModel.GetComponent<MeshRenderer>().sharedMaterial;
        sharedMaterial.EnableKeyword("CLIP_ONE");
        sharedMaterial.DisableKeyword("CLIP_TWO");
        sharedMaterial.DisableKeyword("CLIP_THREE");
        sharedMaterial.SetVector("_planePos", transform.position-new Vector3(0.03f, 0.03f, 0.03f));//add a margin to see the contour of the mesh over the slice
        sharedMaterial.SetVector("_planeNorm", Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(90.0f, 0.0f, 0.0f)) * Vector3.up);//plane normal vector is the rotated 'up' vector.
    }
}
