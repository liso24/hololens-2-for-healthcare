using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

using Dicom;
using Dicom.Imaging;
using Dicom.Log;

public class testHealthcare : MonoBehaviour
{
    public GameObject segmentedModel;
    public Slider indexSlider;
    //public TextAsset fileToOpen;

    private Texture2D df;
    private Vector3 startingPos, startingRot;
    private string _dump;
    private Texture2D _texture;

    // Start is called before the first frame update
    void Start()
    {
        
#if UNITY_EDITOR
        //using (var stream = new MemoryStream(UnityEditor.AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Resources/fo-dicom/Demo/Images/VL3_J2KI.bytes").bytes))
        //using (var stream = File.OpenRead(Application.streamingAssetsPath + "\\VL3_J2KI.bytes"))
#else
        //using (var stream = new MemoryStream(AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Resources/fo-dicom/Demo/Images/VL3_J2KI.bytes").bytes));
#endif
            //var DICOMDIRPath = Application.dataPath + "\\MyAssets\\DICOM";
            using (var stream = File.OpenRead(Application.dataPath + "\\Resources\\MyAssets\\DICOM\\NNI20"))
            //using (var stream = File.OpenRead(AssetDatabase.GetAssetPath(fileToOpen)));
            //using (var stream = new MemoryStream(AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Resources/fo-dicom/Demo/Images/VL3_J2KI.bytes").bytes));

        {
            segmentedModel.GetComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Custom/StandardClippable"))
            {
                hideFlags = HideFlags.HideAndDontSave
            };

            var file = DicomFile.Open(stream);
            var anonymizer = new DicomAnonymizer();
            anonymizer.AnonymizeInPlace(file);

            _dump = file.WriteToString();
            _texture = new DicomImage(file.Dataset).RenderImage().AsTexture2D();

            DisplayFrame(_texture);

            indexSlider.GetComponent<Slider>().maxValue = 0;    //setto il min e max del slider
            indexSlider.GetComponent<Slider>().maxValue = 1;

            
        }
    }

    private void DisplayFrame(Texture2D df)
    {
        this.df = df;
        GetComponent<SpriteRenderer>().sprite = Sprite.Create(df, new Rect(0, 0, df.width, df.height), new Vector2(0.0f, 0.0f));
        //this.df.position = segmentedModel.position;
        //transform.rotation = Quaternion.Euler(df.Transformation.Rotation);
        //transform.localScale = new Vector3(df.Transformation.Scale.x * df.Sprite.pixelsPerUnit, df.Transformation.Scale.y * df.Sprite.pixelsPerUnit, 1);//local scale has additional parameters (rows and columns) for bypassing Unity non - supporting for NPOT at run-time (not required if using quads + textures for

    }

    public void Update()
    {
        //Debug.Log("lo slider ha valore: " + (int)indexSlider.value);
        //Debug.Log("il frame e': " + this.df);
        //Debug.Log("df.Transformation" + this.df.Transformation);

        //transform.position = new Vector3(df.Transformation.Position.x + segmentedModel.transform.position.x, df.Transformation.Position.y + segmentedModel.transform.position.y, df.Transformation.Position.z + segmentedModel.transform.position.z);
        //transform.rotation = Quaternion.Euler(new Vector3(df.Transformation.Rotation.x + segmentedModel.transform.rotation.x, df.Transformation.Rotation.y + segmentedModel.transform.rotation.y, df.Transformation.Rotation.z + segmentedModel.transform.rotation.z));
        // transform.position = segmentedModel.transform.position;
        // transform.rotation = segmentedModel.transform.rotation;
        var sharedMaterial = segmentedModel.GetComponent<MeshRenderer>().sharedMaterial;
        sharedMaterial.EnableKeyword("CLIP_ONE");
        sharedMaterial.DisableKeyword("CLIP_TWO");
        sharedMaterial.DisableKeyword("CLIP_THREE");
        sharedMaterial.SetVector("_planePos", transform.position-new Vector3(0.03f, 0.03f, 0.03f));//add a margin to see the contour of the mesh over the slice
        sharedMaterial.SetVector("_planeNorm", Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(90.0f, 0.0f, 0.0f)) * Vector3.up);//plane normal vector is the rotated 'up' vector.
    }

    
}


